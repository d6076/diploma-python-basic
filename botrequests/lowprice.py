import os

from decouple import config
import requests
import json

RAPID_KEY = config('rapidapi')

if not os.path.exists('temp/'):
    os.mkdir('temp')


def lowprice(_city: str) -> list:
    """Функция на вход получает название города. Результат работы функции список
    Список, содержит данные об отелях в запрашиваемом городе"""

    url = "https://hotels4.p.rapidapi.com/locations/v2/search"
    querystring = {"query": _city, "locale": "en_US"}

    global headers
    headers = {
        'x-rapidapi-host': "hotels4.p.rapidapi.com",
        'x-rapidapi-key': RAPID_KEY
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    response_json = json.loads(response.text)
    destination_id = response_json['suggestions'][0]['entities'][0]['destinationId']

    url = "https://hotels4.p.rapidapi.com/properties/list"

    querystring = {"destinationId": destination_id, "pageNumber": "1", "pageSize": "25", "checkIn": "2020-01-08",
                   "checkOut": "2020-01-15", "adults1": "1", "sortOrder": "PRICE", "locale": "en_US",
                   "currency": "USD"}

    response = requests.request("GET", url, headers=headers, params=querystring)
    response_json = json.loads(response.text)

    hotels = []
    for item in response_json['data']['body']['searchResults']['results']:
        hotels.append({'id': item.get('id'), 'name': item.get('name'), 'star': item.get('starRating'),
                       'price': int(item.get('ratePlan')['price']['current'][1:]),
                       'distance to center': item.get('landmarks')[0]['distance'],
                       'photo': item.get('optimizedThumbUrls')['srpDesktop']})
    sorted(hotels, key=lambda elem: elem.get('price'))

    return hotels


def hotels_count(lst: list, number: int = 10) -> tuple:
    """Функция обрезает количество отелей в списке до значения number, по умолчанию 10, и возвращает строку:
     Название отеля - стоимость проживания"""

    hotels = lst[0:number]
    hotels_low_price = [f'{item["name"]} - price: {"$" + str(item["price"])}' for item in hotels]
    answer = '\n'.join(hotels_low_price)

    return hotels, answer


def load_photo(lst: list, number: int = 3) -> list:
    """ункция получает ссылки на изображения, запрашиваемых отелей, оберезает список до значения number, по умолчанию 3
    и возвращает список. Список, содержит информацию о запрашиваемых отелях вместе
    со ссылками на изображения"""

    url = "https://hotels4.p.rapidapi.com/properties/get-hotel-photos"

    hotels_id = [(item["id"], item['name'], item['price']) for item in lst]

    hotel_photos = []
    for item in hotels_id:

        querystring = {"id": item[0]}

        response = requests.request("GET", url, headers=headers, params=querystring)
        photo_data = json.loads(response.text)['hotelImages'][0:number]

        for item_ in photo_data:
            hotel_photos.append(
                {'id': item[0], 'name': item[1], 'price': item[2],
                 'url': item_['baseUrl'][:-10] + 'z.jpg'})

    return hotel_photos
