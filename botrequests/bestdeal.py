import os

from decouple import config
import requests
import json

RAPID_KEY = config('rapidapi')

if not os.path.exists('temp/'):
    os.mkdir('temp')


def bestdeal(_city: str) -> list:
    """Функция на вход получает название города. Функция из первого запроса забирает общее количество найденных отелей.
    После этого вычесляет общее количество страниц и запрашивает каждую страницу.
    Возвращает список"""

    url = "https://hotels4.p.rapidapi.com/locations/v2/search"
    querystring = {"query": _city, "locale": "en_US"}

    global headers
    headers = {
        'x-rapidapi-host': "hotels4.p.rapidapi.com",
        'x-rapidapi-key': RAPID_KEY
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    response_json = json.loads(response.text)
    destination_id = response_json['suggestions'][0]['entities'][0]['destinationId']

    url = "https://hotels4.p.rapidapi.com/properties/list"

    pages_count = 3

    hotels = []
    for i in range(1, pages_count + 1):
        querystring = {"destinationId": destination_id, "pageNumber": i, "pageSize": "25", "checkIn": "2020-01-08",
                       "checkOut": "2020-01-15", "adults1": "1", "sortOrder": "PRICE", "locale": "en_US",
                       "currency": "USD"}
        response = requests.request("GET", url, headers=headers, params=querystring)
        response_json = json.loads(response.text)

        for item in response_json['data']['body']['searchResults']['results']:
            hotels.append({'id': item.get('id'), 'name': item.get('name'), 'star': item.get('starRating'),
                           'price': int(item.get('ratePlan')['price']['current'][1:]),
                           'distance to center': float(item.get('landmarks')[0]['distance'][:-6]),
                           'photo': item.get('optimizedThumbUrls')['srpDesktop']})
        sorted(hotels, key=lambda elem: elem.get('price'))

    return hotels


def hotel_price(lst: list, price_range: str) -> list:
    """Функция, на вход получает диапазон цен строкой в виде 10-20. Находит подходящие границы и обрезает список
    , в соответсвии с ними"""

    price_range = price_range.split('-')
    if int(price_range[0]) > int(price_range[1]):
        price_range[0], price_range[1] = price_range[1], price_range[0]

    price_list = [item['price'] for item in lst]

    left_border = list(filter(lambda x: x <= int(price_range[0]), price_list))
    if len(left_border) == 0:
        left_border = price_list[0]
    else:
        left_border = max(left_border)
    left_border_index = price_list.index(left_border)

    right_border = list(filter(lambda x: x > int(price_range[1]), price_list))
    if len(right_border) == 0:
        right_border = price_list[len(price_list) - 1]
    else:
        right_border = min(right_border)
    right_border_index = price_list.index(right_border)

    hotels_reduced = lst[left_border_index:right_border_index + 1]

    return hotels_reduced


def hotel_distance(lst: list, distance_range: str) -> list:
    """Функция, на вход получает диапазон расстояний строкой в виде 0.1-20. Находит подходящие границы и обрезает список
        , в соответсвии с ними"""

    distance_range = distance_range.split('-')
    if float(distance_range[0]) > float(distance_range[1]):
        distance_range[0], distance_range[1] = distance_range[1], distance_range[0]

    lst.sort(key=lambda elem: elem.get('distance to center'))

    distance_list = [item['distance to center'] for item in lst]

    left_border = list(filter(lambda x: x <= float(distance_range[0]), distance_list))
    if len(left_border) == 0:
        left_border = distance_list[0]
    else:
        left_border = max(left_border)
    left_border_index = distance_list.index(left_border)

    right_border = list(filter(lambda x: x > float(distance_range[1]), distance_list))
    if len(right_border) == 0:
        right_border = distance_list[len(distance_list) - 1]
    else:
        right_border = min(right_border)
    right_border_index = distance_list.index(right_border)

    hotels_reduced = lst[left_border_index:right_border_index + 1]

    return hotels_reduced


def hotels_count(lst: list, number: int = 10) -> tuple:
    """Функция обрезает количество отелей в списке до значения number, по умолчанию 10, и возвращает строку:
     Название отеля - стоимость проживания"""

    hotels = lst[0:number]
    hotels_low_price = [f'{item["name"]} - {str(item["distance to center"])} мили - price: {"$" + str(item["price"])}'
                        for item in hotels]
    answer = '\n'.join(hotels_low_price)

    return hotels, answer


def load_photo(lst: list, number: int = 3) -> list:
    """Функция получает ссылки на изображения, запрашиваемых отелей, оберезает список до значения number, по умолчанию 3
    и возвращает список. Список, содержит информацию о запрашиваемых отелях вместе
    со ссылками на изображения"""

    url = "https://hotels4.p.rapidapi.com/properties/get-hotel-photos"

    hotels_id = [(item["id"], item['name'], item['price'], item['distance to center']) for item in lst]

    hotel_photos = []
    for item in hotels_id:

        querystring = {"id": item[0]}

        response = requests.request("GET", url, headers=headers, params=querystring)
        photo_data = json.loads(response.text)['hotelImages'][0:number]

        for item_ in photo_data:
            hotel_photos.append(
                {'id': item[0], 'name': item[1], 'price': item[2],
                 'url': item_['baseUrl'][:-10] + 'z.jpg', 'distance to center': item[3]})

    return hotel_photos
