Too Easy Travel Bot 🤖
Бот написан на языке Python
Eсли вы нашли ошибку - пожалуйста, свяжитесь со мной в телеграмме @awameg
Telegram link @GenadiyTooEasyTravelbot

Использованны следующие библиотеки: ✏️

    pyTelegramBotAPI==4.1.1
    requests==2.26.0
    telebot==0.0.4
    python-decouple==3.5

Что может делать этот бот?
    
    * Узнать топ самых дешёвых отелей в городе (команда /lowprice).
    * Узнать топ самых дорогих отелей в городе (команда /highprice).
    * Узнать топ отелей, наиболее подходящих по цене и расположению от центра (команда /bestdeal).
    * Узнать историю поиска отелей (команда /history)
    * Получить справку по коммандам (комманда /help)

Установка:

Создайте нового бота у @BotFather.
Создайте файл .env и поместить в него токин, для доступа к боту и rapidapi

    token=
    rapidapi=

Установите необходимые библеотеки:
    
    pip install requirements.txt

или

    python -m pip install -r requirements.txt

В директории с файлами бота необходимо создать виртуальное окружение:
    
    pip -m venv venvBOT

И активировать его:

    source venvBOT/bin/activate

Затем запустите бота коммандой:
    
    python3 main.py

© 2021 Удальцов Ростислав.
